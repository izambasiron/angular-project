'use strict';

angular
  .module('challengeApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/game-of-life/', {
        templateUrl: 'views/game-of-life.html',
        controller: 'GameOfLifeCtrl'
      })

      .when('/2048/', {
        templateUrl: 'views/2048.html',
        controller: '2048Ctrl'
      })

      .when('/url-shortener/', {
        templateUrl: 'views/url-shortener.html',
        controller: 'UrlShortenerCtrl'
      })

      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })

      .otherwise({
        redirectTo: '/'
      });
  });
