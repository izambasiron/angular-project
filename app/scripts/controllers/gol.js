'use strict';

var challengeAppControllers = angular.module('challengeApp');
challengeAppControllers
  .controller('GameOfLifeCtrl', function ($scope) {
    GLOBALS['context'] = add_canvas(document.getElementById('life'));

    init_sim();
    GLOBALS['interval_id'] = window.setInterval(run_sim, GLOBALS['interval']);

    window.onkeypress = on_key_press;
  });
