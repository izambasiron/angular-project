'use strict';

var challengeAppControllers = angular.module('challengeApp');
challengeAppControllers
  .factory('ShortApi', function($resource) {
    var ShortApi = $resource('http://localhost:4567/s', {}, {
      'save': {method:'POST'}
    });
    return ShortApi;
  });

challengeAppControllers
  .controller('UrlShortenerCtrl', function ($scope, ShortApi) {
    $scope.shortenUrl = function() {
      var shortenApi = new ShortApi();
      shortenApi.url = $scope.longUrl;
      shortenApi.$save({}, function(data){
        $scope.shortUrl = data['url'];
      });
    }
  });
