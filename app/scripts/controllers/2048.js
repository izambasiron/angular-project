'use strict';

var challengeAppControllers = angular.module('challengeApp');
challengeAppControllers
  .factory('ScoreApi', function($resource) {
    var Score = $resource('http://localhost:4567/2048/highscores/:id', {id: '@id'}, {
      'get': {method:'GET'},
      'update': {method: 'POST'}
    });

    var WinCount = $resource('http://localhost:4567/2048/highscores/:id/win', {id: '@id'}, {
      'get': {method: 'GET', isArray: false}
    });

    var LossCount = $resource('http://localhost:4567/2048/highscores/:id/loss', {id: '@id'}, {
      'get': {method: 'GET', isArray: false}
    });

    return {
      Score: Score,
      WinCount: WinCount,
      LossCount: LossCount
    };
  });

challengeAppControllers
  .controller('2048Ctrl', function ($scope, ScoreApi) {
    $scope.aiCurrentScore = 0;
    $scope.aiScores = [0];
    $scope.humanCurrentScore = 0;
    $scope.humanScores = [0];
    $scope.humanWinCount = 0;
    $scope.humanLossCount = 0;
    $scope.aiWinCount = 0;
    $scope.aiLossCount = 0;

    // Wait till the browser is ready to render the game (avoids glitches)
    window.requestAnimationFrame(function () {
      new GameManager(4, KeyboardInputManager, HTMLActuator, LocalStorageManager);

      var ai = new AiGameManager(4, AiKeyboardInputManager, AiHTMLActuator, LocalStorageManager);
      if (!ai.running) {
        ai.running = true;
        ai.run();
      }
    });

    $scope.getScores = function(player) {
      var scoreApi = new ScoreApi.Score();
      var aiScore = scoreApi.$get({id: player}, function(data) {
        if (player == 'hoominz')
          $scope.humanScores = data['scores'].reverse();
        else
          $scope.aiScores = data['scores'].reverse();
      });
    };

    $scope.$watch("aiCurrentScore", function(newValue, oldValue){
      if ($scope.aiCurrentScore == 0)
        return;
      var scoreApi = new ScoreApi.Score();
      scoreApi.won = $scope.aiCurrentWon;
      scoreApi.score = $scope.aiCurrentScore;
      scoreApi.$update({id: 'ai'}, function() {
        $scope.getScores('ai', $scope.aiScores);
        if ($scope.aiCurrentWon)
          $scope.getWinCount('ai', $scope.aiWinCount);
        else
          $scope.getLossCount('ai', $scope.aiLossCount);
      });
    });

    $scope.$watch("humanCurrentScore", function(newValue, oldValue){
      if ($scope.humanCurrentScore == 0)
        return;
      var scoreApi = new ScoreApi.Score();
      scoreApi.won = $scope.humanCurrentWon;
      scoreApi.score = $scope.humanCurrentScore;
      scoreApi.$update({id: 'hoominz'}, function() {
        $scope.getScores('hoominz', $scope.humanScores);
        if ($scope.humanCurrentWon)
          $scope.getWinCount('hoominz', $scope.humanWinCount);
        else
          $scope.getLossCount('hoominz', $scope.humanLossCount);
      });
    });

    $scope.getWinCount = function(player) {
      var winCount = new ScoreApi.WinCount();
      winCount.$get({id: player}, function(data) {
        if (player == 'hoominz')
          $scope.humanWinCount = data['win'];
        else
          $scope.aiWinCount = data['win'];
      });
    };

    $scope.getLossCount = function(player) {
      var lossCount = new ScoreApi.LossCount();
      lossCount.$get({id: player}, function(data) {
        if (player == 'hoominz')
          $scope.humanLossCount = data['loss'];
        else
          $scope.aiLossCount = data['loss'];
      });
    };

    $scope.getScores('hoominz');
    $scope.getScores('ai');
    $scope.getWinCount('hoominz');
    $scope.getLossCount('hoominz');
    $scope.getWinCount('ai');
    $scope.getLossCount('ai');
  });
