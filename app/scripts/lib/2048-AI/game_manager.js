function AiGameManager(size, InputManager, Actuator, StorageManager) {
  this.size           = size; // Size of the grid
  this.inputManager   = new InputManager;
  this.actuator       = new Actuator;
  this.storageManager = new StorageManager;

  this.running      = false;

  this.inputManager.on("move", this.move.bind(this));
  this.inputManager.on("restart", this.restart.bind(this));

  this.inputManager.on('think', function() {
    var best = this.ai.getBest();
    this.actuator.showHint(best.move);
  }.bind(this));


  this.inputManager.on('run', function() {
    if (this.running) {
      this.running = false;
      this.actuator.setRunButton('Auto-run');
    } else {
      this.running = true;
      this.run()
      this.actuator.setRunButton('Stop');
    }
  }.bind(this));

  this.setup();
}

// Restart the game
AiGameManager.prototype.restart = function () {
  this.actuator.restart();
  this.setup();
  this.run();
};

// Set up the game
AiGameManager.prototype.setup = function () {
  this.grid         = new AiGrid(this.size);
  this.grid.addStartTiles();

  this.ai           = new AI(this.grid);

  this.score        = 0;
  this.over         = false;
  this.won          = false;

  // Update the actuator
  this.actuate();
};


// Sends the updated grid to the actuator
AiGameManager.prototype.actuate = function () {
  if (this.storageManager.getBestAiScore() < this.score) {
    this.storageManager.setBestAiScore(this.score);
  }

  this.actuator.actuate(this.grid, {
    score: this.score,
    over:  this.over,
    won:   this.won,
    bestScore:  this.storageManager.getBestAiScore()
  });

  if (this.won || this.over) {
    var c = document.getElementsByClassName("game-container")[0];
    var score = this.score;
    var won = this.won;
    $scope = angular.element(c).scope();
    $scope.$apply(function(){
      $scope.aiCurrentWon = won;
      $scope.aiCurrentScore = score;
    });
  }

  // Restart if in a jam.
  var timeout = animationDelay;
  if (this.over && !this.won) {
    var self = this;
    setTimeout(function(){
      self.restart();
    }, timeout);
  }
};

// makes a given move and updates state
AiGameManager.prototype.move = function(direction) {
  var result = this.grid.move(direction);
  this.score += result.score;

  if (!result.won) {
    if (result.moved) {
      this.grid.computerMove();
    }
  } else {
    this.won = true;
  }

  //console.log(this.grid.valueSum());

  if (!this.grid.movesAvailable()) {
    this.over = true; // Game over!
  }

  this.actuate();
}

// moves continuously until game is over
AiGameManager.prototype.run = function() {
  var best = this.ai.getBest();
  this.move(best.move);
  var timeout = animationDelay;
  if (this.running && !this.over && !this.won) {
    var self = this;
    setTimeout(function(){
      self.run();
    }, timeout);
  }
}
