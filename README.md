# Angular Project

## Clone Angular Project

```
$ git clone git@bitbucket.org:izambasiron/angular-project.git
$ cd angular-project
```

## Use 'package.json' to pull down the app's dependencies from NPM
```
$ npm install
```

## Use 'bower.json' to pull down the app's dependencies from Bower
```
$ bower install
```

## Run 
```
$ grunt serve
```
This will open up a browser that points to http://127.0.0.1:9000/#/

## Clone Sinatra Project
Open another terminal. Make sure you have redis installed

```
$ git clone git@bitbucket.org:izambasiron/sinatra-project.git
$ cd sinatra-project
$ bundle install
$ rackup -p 4567
```

## Source
1. 2048 by [Gabriele Cirulli](https://github.com/gabrielecirulli/2048)
2. 2048-AI by [ov3y](https://github.com/ov3y/2048-AI)
3. Ruby URL Shortener [Google](https://www.google.com.my/search?q=ruby+url+shortener) 
4. Conway's Game of Life by [Matt Davis](https://github.com/jiffyclub/game-of-life-js)